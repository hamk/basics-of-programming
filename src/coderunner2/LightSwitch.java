package coderunner2;

public class LightSwitch {

    boolean lightsOn = false;

    void printInfo() {
        System.out.println("Lights are " + ((lightsOn) ? "on" : "off"));
    }

}
