package finalassignment2021;

public class Salmon extends Fish {


    public Salmon(double fishLength, double fishWeight) {
        super(fishLength, fishWeight);
        poison = false;
    }

    @Override
    public String fishInfo() {
        return super.fishInfo() + "\nYou fished a Salmon!";
    }
}
