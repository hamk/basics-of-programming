package finalassignment2021;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random r = new Random();

        int numberOfSalmons = 4;

        for (int i = 0; i < 20; i++) {
            if (i < numberOfSalmons)
                FishMan.youCaught(new Salmon(r.nextDouble() * 20.0, r.nextDouble() * 20.0));
            else
                FishMan.youCaught(new PoisonFish(r.nextDouble() * 20.0, r.nextDouble() * 20.0));
        }

        FishMan.myBasket();
        FishMan.fishDayInfo();
    }

}
