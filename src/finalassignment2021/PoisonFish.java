package finalassignment2021;

public class PoisonFish extends Fish {

    public PoisonFish(double fishLength, double fishWeight) {
        super(fishLength, fishWeight);
        poison = true;
    }

    @Override
    public String fishInfo() {
        return super.fishInfo() + "\nYou fished a Poison fish!";
    }
}
