package arrays.basics;

public class Arrays001 {

    public static void main(String[] args) {

        // here, we create our array and initialize it with the correct values
        String[] array = {"Green", "Blue", "Yellow"};

        // we use index "1" and not "2" because in Java, arrays start counting at 0. That means that when we want
        // the second element, we count 0, 1... Thus, index 1 is actually the second element inside an array.
        System.out.println(array[1]);

        // This is called an enhanced loop. We can read this statement as "for every String 's' inside array,
        // print the String"
        for (String s : array) {
            System.out.println(s);
        }

    }

}
