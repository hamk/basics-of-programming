package arrays.basics;

import java.util.ArrayList;
import java.util.Collections;

public class Arrays007 {

    public static void main(String[] args) {

        ArrayList<String> cars = new ArrayList<>();
        cars.add("Kia");
        cars.add("Tesla");
        cars.add("BMW");
        cars.add("Renault");

        // 1. Print the cars ArrayList
        for (String car : cars)
            System.out.println(car);

        // 2. Insert Ford and Kia

        // we find the index of Kia
        int KIAindex = cars.indexOf("Kia");

        // We add the car Ford right after the Kia (Kia's index + 1)
        cars.add(KIAindex + 1, "Ford");

        // 3. Delete Tesla
        cars.remove("Tesla");

        // 4. Change BMW to Audi
        int BMWindex = cars.indexOf("BMW");
        cars.set(BMWindex, "Audi");

        // 5. Print "MODIFIED LIST"
        System.out.println("MODIFIED LIST");

        // 6. Print the whole list
        for (String car : cars)
            System.out.println(car);

        // 7. Sort the list (we use the Collections library to sort this list)
        Collections.sort(cars);

        // 8. Print "SORTED LIST"
        System.out.println("SORTED LIST");

        // 9. Print the whole list
        for (String car : cars)
            System.out.println(car);


    }

}
