package arrays.basics;

public class Arrays003 {

    public static void main(String[] args) {

        String[] array = {"Green", "Blue", "Yellow"};
        System.out.println(array[1]);

        for (int i = 0; i < array.length; i++) {
            System.out.println((i + 1) + ". " + array[i]);
        }

    }

}
