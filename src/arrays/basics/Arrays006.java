package arrays.basics;

public class Arrays006 {

    public static void main(String[] args) {

        // here, we create our array and initialize it with the correct values
        int[] array = {16, 18, 5, 3, 10};


        /*
         * Let me explain the algorithm. First we will create a variable called "minimum" and we initialize it to the
         * first value in the array. Then, we loop through all the variables inside the array. We compare each variable
         * to this "minimum" variable. If that variable is smaller than this current "minimum", we set minimum to this
         * new value. At the end, we print the minimum.
         */
        int minimum = array[0];

        for (int i : array) {
            if (i < minimum)
                minimum = i;
        }

        System.out.println(minimum);

    }

}
