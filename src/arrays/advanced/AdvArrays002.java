package arrays.advanced;

import java.util.ArrayList;
import java.util.Scanner;

public class AdvArrays002 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // we use an ArrayList because ArrayLists don't have a fixed size. We don't know how many items is the user
        // going to add beforehand and thus we use an ArrayList to compensate fir it.
        ArrayList<String> items = new ArrayList<>();


        // we use a "while (true)" loop because it will loop forever because the condition inside is always true. It's
        // the same as if we typed something like "while (1 + 1 == 2)" which is also always true since 1+1 is always 2.
        while (true) {
            System.out.println("Add item (x = exit)");

            // get the input from the user
            String input = in.nextLine();

            // if the user types "x", we break out of this neverending while loop
            if (input.equals("x")) {
                break;
            }
            items.add(input);
        }

        for (String item : items) {
            System.out.println(item);
        }
    }

}
