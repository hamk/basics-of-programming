package staticmethodsandoverloading;

class Car {

    int avgMass = 50_000;
    String type;

    static int carCount = 0;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMass() {
        return avgMass;
    }

    public void setMass(int mass) {
        this.avgMass = mass;
    }

    public Car(int mass, String type) {
        this.avgMass = mass;
        this.type = type;

        carCount++;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
