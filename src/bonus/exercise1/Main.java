package bonus.exercise1;

public class Main {

    public static void main(String[] args) {
        Car car = new Car();
        Car car1 = new Car("First car", "Prius");
        Car car2 = new Car("Second car", "BMW");
        car.tank(5);
        car1.tank(4);
        car2.tank(-5);
        car.accelerate();
        car1.accelerate();
        car2.accelerate();
        ElectricCar electricCar = new ElectricCar("First EV", "Tesla");
        ElectricCar secondElectricCar = new ElectricCar("Second EV", "Hyundai");
        secondElectricCar.tank(5);
    }

}
