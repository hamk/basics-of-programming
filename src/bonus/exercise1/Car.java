package bonus.exercise1;

public class Car {
    private String name, brand;
    private double range;

    Car() {
        name = "Default Name";
        brand = "Default Brand";
        range = 0.0;
        System.out.println("I am a Default Car.");
        System.out.println("My name is " + name + "and my brand is " + brand + " and my range is " + range);
    }

    Car(String name, String brand) {
        this.name = name;
        this.brand = brand;
    }

    double tank(double input) {
        if (input > 0)
            range = input * 5;
        else {
            System.out.println("Tank amount cannot be zero!");
            return range = 0;
        }
        return range;
    }

    void range() {
        System.out.println("The car range is " + range + ".");
    }

    protected void accelerate() {
        if (range > 0)
            System.out.println("You are accelerating.");
        else
            System.out.println("Make sure your tank is full.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getRange() {
        return range;
    }

    public void setRange(double range) {
        this.range = range;
    }
}
