package classes.car005;

import java.util.Scanner;

// you have to make this class public, idk why but CodeRunner gives errors otherwise
public class Car {

    private String brand;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    private String model;
    private int amountOfFuel;

    public Car() {
        brand = "";
        model = "";
        amountOfFuel = 0;

        printData();
    }

    public Car(String brand, String model, int amountOfFuel) {
        this.brand = brand;
        this.model = model;
        this.amountOfFuel = amountOfFuel;

        printData();
    }

    public void brake() {
        System.out.println("Car is breaking");
    }

    public void accelerate() {
        if (amountOfFuel > 0) System.out.println("Car is accelerating");
        amountOfFuel--;
    }

    private void printData() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Fuel: " + amountOfFuel);
    }

    public void refuel(int amount) {
        System.out.println("Fuel in the tank: " + amountOfFuel);
        System.out.println("Refuel: " + amount);

        amountOfFuel += amount;

        System.out.println("Fuel in the tank after the refuel: " + amountOfFuel);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        Car car = new Car("Toyota", "RAV4", 40);

        mainloop:
        while (true) {
            System.out.println("a=accelerate b=break x=exit");
            String input = s.nextLine();
            switch (input) {
               case  "a":
                   car.accelerate();
                   break;
                case "b":
                    car.brake();
                    break;
                case "x":
                    break mainloop;
            }
        }

    }
}
