package classes.car001;

class Car {

    String brand;
    String model;
    int amountOfFuel;

    public void brake() {
        System.out.println("Car is breaking");
    }

    public void accelerate() {
        if (amountOfFuel > 0) System.out.println("Car is accelerating");
        amountOfFuel--;
    }

    public void printData() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Fuel: " + amountOfFuel);
    }

}
