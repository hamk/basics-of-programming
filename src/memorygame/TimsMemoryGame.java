package memorygame;

import java.util.Random;
import java.util.Scanner;

public class TimsMemoryGame {

    // !!! UNFINISHED !!!

    public static void main(String[] args) {
        /* *******  Giving instructions and rules to the user ***** */
        try {
            System.out.println("******** Number Memory Game ********");
            // delay 1 seconds
            Thread.sleep(1000);
            System.out.println("Try to remember next 7 numbers");
            // delay 1 seconds
            Thread.sleep(1000);
            System.out.println("You have 5 seconds to remember these numbers!");
            // delay 1 seconds
            Thread.sleep(1000);
            System.out.println("After 5 seconds the numbers will disappear from screen!");
            // delay 0.5 seconds
            Thread.sleep(500);
            System.out.println("*************** Ready? *************");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        /*  Initializing user input Scanner & Random Number Generator ***** */
        Random random = new Random();
        Scanner in = new Scanner(System.in);

        // Declaring variable to use to set initial value
        int i = 0;
        int[] randomN = {
                random.nextInt(5) + 1, random.nextInt(5) + 1, random.nextInt(5) + 1,
                random.nextInt(5) + 1, random.nextInt(5) + 1, random.nextInt(5) + 1,
                random.nextInt(5) + 1
        };
        int[] myNumbers = {
                randomN[0], randomN[1], randomN[2], randomN[3], randomN[4],
                randomN[5], randomN[6]
        };

        // print out the numbers to remember
        for (i = 0; i < myNumbers.length; i++) {
            System.out.print(myNumbers[i]);
        }

        try {// delay 5 seconds
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        // Printing out 20 empty lines
        for (i = 0; i < 20; ++i) System.out.println();
        // Guessing numbers
        String[] vastaus;
        vastaus = new String[7];
        System.out.println("Type given numbers in the same order as introduced.");
        System.out.print("1. number:");
        vastaus[0] = in.nextLine();

        System.out.print("2. number:");
        vastaus[1] = in.nextLine();

        // more answers!
        // Checking numbers
        System.out.println("Your answer:");
        System.out.println(vastaus[0] + vastaus[1] + vastaus[2] + vastaus[3] + vastaus[4] + vastaus[5] + vastaus[6]);
        //System.out.println(vastaus[i]);
    }
}