package finalooptest;

import java.util.Scanner;

public class TvApp {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("REMOTE CONTROL");
        Tv tv = new Tv();
        while (true) {
            System.out.println("Channel: 1-9, Shutdown: 0");
            int channel = s.nextInt();
            if (channel == 0)
                break;
            tv.changeChannel(channel);
        }
        tv.shutdown();
    }

}

class Tv {
    private int channel;
    private boolean isOn;

    public int getChannel() {
        return channel;
    }

    public Tv() {
        channel = 1;
        isOn = true;
    }

    void shutdown() {
        isOn = false;
        System.out.println("TV is off");
    }

    void changeChannel(int channel) {
        this.channel = channel;
        System.out.println("Channel is " + channel);
    }
}