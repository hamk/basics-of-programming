package luckygame;

import java.util.Random;
import java.util.Scanner;

public class LuckyGame {

    // we have to declare the static variable money here because we're using it in the method "editMoney()".
    static int money;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Random random = new Random();

        // ask how much money the user wants at the beginning
        System.out.print("How much money do you want in the beginning (€)? ");

        // we first do nextLine() and then we convert it into an integer. It's done like this because if we use
        // nextInt(), then the newline character gets caught up inside the scanner, and it will cause problems later on.
        money = Integer.parseInt(sc.nextLine());

        // check if money amount is normal
        if (money < 1) {
            System.out.println("You're too poor to play!");

            // return inside the main function means exit program
            return;
        } else if (money > 1000) {
            System.out.println("You're too rich for this game!");
            return;
        }

        System.out.println();

        // This is the main game loop. Notice the "gameloop:" sign here. It's called a "label" in Java. It's a way
        // to kind of mark a certain loop. We'll use it later.
        gameloop:
        while (true) {
            // tell the user amount money change. Subtract 1 euro for the game
            editMoney(-1, "starting a new game");
            wait(4000);

            // write start text
            System.out.println("Prepare for the three numbers!!!\n");
            wait(2000);

            // variable to hold the number of lucky numbers
            int luckyCount = 0;

            // generate three numbers
            for (int i = 0; i < 3; i++) {
                int luckyNumber = giveLuckyNumber(random);

                // using the Elvis operator. Search it up. It's basically a fancy way of saying "if number equals to 7,
                // add 1 to luckyCount. Otherwise, add 0.
                luckyCount += (luckyNumber == 7) ? 1 : 0;

                System.out.print(luckyNumber);
                wait(500);
            }

            wait(1000);

            // We use the Elvis operator to add an "s" after "numbers"
            System.out.println("\n\nYou got " + luckyCount + " number" + ((luckyCount == 1) ? "" : "s") + " lucky!");
            int prize = luckyCount * 2; // if luckyCount == 1, then prize 2. If luckyCount == 2, then prize 4 etc...

            // tell the user about the money state, add money to his account
            if (prize > 0) editMoney(prize, "winning the prize");

            // implemented this quick while loop in case the user makes a typo in the letters Y or N
            while (true) {
                System.out.print("Do you want to play again? (Y/n) ");
                String decision = sc.nextLine();

                // convert the decision to lowercase (thus, Y becomes y, N becomes n)
                decision = decision.toLowerCase();

                if (decision.equals("n")) {
                    System.out.println("\nTHANK YOU FOR PLAYING! YOUR CASHOUT IS " + money + "€!");
                    /* Since we're inside this special loop inside the main gameloop, using "break" would break us
                     * out of the inner loop. But what if we want to break out of the outer game loop? No worries.
                     * Labels got our backs. Since we specify that we want to specifically break out of the loop
                     * with the label "gameloop", Java breaks out of the outer loop. */
                    break gameloop;
                } else if (decision.equals("y")) {
                    System.out.println("========================================\n\n");
                    // Now, we only break out of the inner loop and not out of the game loop. Thus, starting the game again.
                    break;
                } else {
                    System.out.println("Incorrect choice, try again.");
                    wait(1000);
                }
            }
        }
    }


    // This here is an example of proper Java documentation. If you want to learn more about the documentation,
    // head over to https://www.tutorialspoint.com/java/java_documentation.htm
    /**
     * This method takes our Random object and uses it to generate a random number. It's done like this because
     * it's way better to just give the method the Random object instead of creating it inside the method everytime.
     * Also, it's a "private" method, which basically means, that you can ONLY use it inside this class LuckyGame.
     * If you were to use it somewhere else (inside another class), the compiler would error.
     * Also, notice the method is "static". It's like that on purpose. The method has to be static because it is used
     * inside the static main method. A static method can only call another static method (unless you create objects
     * but that's for later...).
     *
     * @param random The random object.
     * @return Returns the random value from 1 to 9.
     */
    private static int giveLuckyNumber(Random random) {
        // nextInt(9) generates a number from 0 to 8 and then we add 1, thus shifting the entire thing to the right
        // from 1 till 9.
        return random.nextInt(9) + 1;
    }

    /**
     * Waits for a certain time.
     *
     * @param milliseconds Time in milliseconds to wait.
     */
    private static void wait(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Method for managing money. Anytime the program edits the amount of money, a message is displayed informing the
     * user about the current amount. Notice you can subtract money by adding a negative value.
     * @param amount Amount of money to be added or subtracted.
     * @param reason Reason for the addition/subtraction
     */
    private static void editMoney(int amount, String reason) {
        money += amount;

        // FANCY ALERT! Using a very fancy implementation of the elvis operator.
        System.out.println(
                ((amount > 0) ? "Added " : "Subtracted ") +
                ((amount > 0) ? amount : amount * -1) + "€ for " + reason + ". " +
                "There is " + money + "€ left."
        );
    }

}
