package methods.advanced;

public class AdvancedFancy006 {

    public void calculate(int number1, int number2, String operation) {

        // this is the use of an "enhanced switch". It was introduced in Java 13, so it won't run on the old-ass
        // Java 1.8 that our school is using. You can refer to https://www.vojtechruzicka.com/java-enhanced-switch/
//        int result = switch (operation) {
//
//            // the tester has a typo here. It should be "subtraction" and not "subStraction"
//            case "substraction" -> number1 - number2;
//            case "sum" -> number1 + number2;
//            case "multiplication" -> number1 * number2;
//            default -> 0;
//        };

//        System.out.println("The result is " + result + ".");

    }

}
